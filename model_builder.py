from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Dropout
from keras.models import load_model
import matplotlib.pyplot as plt

class ModelBuilder:
    def __init__(self):
        self.build()

    def build(self):    
        # Initialising the CNN
        self.classifier = Sequential()

        self.classifier.add(Conv2D(32, (3, 3), input_shape = (64, 64, 3), activation = 'relu'))

        self.classifier.add(MaxPooling2D(pool_size = (2, 2)))

        self.classifier.add(Conv2D(64, (3, 3), activation = 'relu'))
        self.classifier.add(MaxPooling2D(pool_size = (2, 2)))

        self.classifier.add(Conv2D(128, (3, 3), activation = 'relu'))
        self.classifier.add(MaxPooling2D(pool_size = (2, 2)))
        self.classifier.add(Conv2D(128, (3, 3), activation = 'relu'))
        self.classifier.add(MaxPooling2D(pool_size = (2, 2)))

        self.classifier.add(Flatten())
        self.classifier.add(Dropout(0.5))
        
        self.classifier.add(Dense(units = 512, activation = 'relu'))
        
        
        self.classifier.add(Dense(units = 1, activation = 'sigmoid'))
        
        self.classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

    def getModel(self):
        return self.classifier

    def train(self):
        from keras.preprocessing.image import ImageDataGenerator

        train_datagen = ImageDataGenerator(rescale = 1./255,
                                           shear_range = 0.2,
                                           zoom_range = 0.2,
                                           horizontal_flip = True,
                                           rotation_range=15,)

        test_datagen = ImageDataGenerator(rescale = 1./255)

        training_set = train_datagen.flow_from_directory('dataset/training_set',
                                                         target_size = (64, 64),
                                                         batch_size = 32,
                                                         class_mode = 'binary')

        test_set = test_datagen.flow_from_directory('dataset/test_set',
                                                    target_size = (64, 64),
                                                    batch_size = 32,
                                                    class_mode = 'binary')

        history = self.classifier.fit(training_set,
                                 steps_per_epoch = 5696/32,
                                 epochs = 200,
                                 validation_data = test_set,
                                 validation_steps = 1687/32)
        print(history.history.keys())
        plt.plot(history.history['accuracy'])
        plt.plot(history.history['val_accuracy'])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()

        self.classifier.save('pug_recognition_model.h5')
        

        print ("success training!")
        i = input("\nPress enter to stop.")

if __name__ == "__main__":
    model = ModelBuilder()
    model.train()
