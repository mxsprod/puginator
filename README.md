# Puginator

Load a dog image, and I will tell you if it's a pug or not!


I used a dataset of ~2500 pugs images and ~5000 other dogs to build the model (accuracy ~80%). You can try to build your model with your own dataset by changing classifier.fit properties and launch model_builder as main.