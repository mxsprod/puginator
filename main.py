import tkinter as tk
import tkinter.filedialog as tkfd
from PIL import Image, ImageTk
from model_builder import ModelBuilder
from keras.models import load_model
import numpy as np
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator

class MainGui:
    def __init__(self):
        self.classifier = load_model('model/pug_recognition_model.h5')
        self.CANVAS_WIDTH = 300
        self.CANVAS_HEIGHT = 300
        self.model = ModelBuilder()
        #self.model = ModelBuilder()
        
        self.root =tk.Tk()
        self.root.title("Is this doggo a pug?")
        self.root.iconbitmap("img/puginator.ico")
        self.root.geometry("600x400")
        self.root.resizable(False, False)

        
        self.leftcanvas = tk.Canvas(self.root, width = self.CANVAS_WIDTH, height = self.CANVAS_HEIGHT, bg = "white")
        self.rightcanvas = tk.Canvas(self.root, width = self.CANVAS_WIDTH, height = self.CANVAS_HEIGHT, bg = "white")
        self.searchbut = tk.Button(text = "Search doggo", command = self.openImage).grid(row = 1, column = 0, pady = 30)
        self.predict = tk.Button(text = "Is this doggo a pug?", command = self.process).grid(row = 1, column = 1, pady = 30)

        
        self.leftcanvas.grid(row = 0, column = 0)
        self.rightcanvas.grid(row = 0, column = 1)

        
        self.root.mainloop()

    def openImage(self):
        try:
            self.filepath = tkfd.askopenfilename(title = "Where is your doggo???...", filetypes = (("JPEG files", "*.jpg"), ("PNG files", "*.png"), ("GIF files", "*.gif"), ("All files", "*.*")))
        
            image = Image.open(self.filepath)
            width, height = image.size
            if width > self.CANVAS_WIDTH or height > self.CANVAS_HEIGHT: #adapt image size to window
                if width >= height: 
                    basewidth = self.CANVAS_WIDTH
                    wpercent = (basewidth / float(width))
                    hsize = int((float(height) * float(wpercent)))
                    image = image.resize((basewidth, hsize), Image.ANTIALIAS)
                else:
                    baseheight = self.CANVAS_HEIGHT
                    hpercent = (baseheight / float(height))
                    wsize = int(float(width) * float(hpercent))
                    image = image.resize((wsize, baseheight), Image.ANTIALIAS)
            
            self.displayedImage = ImageTk.PhotoImage(image)
            self.leftcanvas.create_image(self.CANVAS_WIDTH/2, self.CANVAS_HEIGHT/2, image = self.displayedImage)
            self.rightcanvas.delete('all')
        except AttributeError:
            pass

    def process(self):
        test_image = image.load_img(self.filepath, target_size = (64, 64))
        test_image = image.img_to_array(test_image)
        test_image = np.expand_dims(test_image, axis = 0)
        result = self.classifier.predict(test_image)
        #training_set.class_indices
        if result[0][0] > 0.5:
            prediction = 'pug'
        else:
            prediction = 'other'

        if prediction == 'pug':
            img = Image.open("img/validation.jpg") 
            self.photo = ImageTk.PhotoImage(img) 
        else:
            img = Image.open("img/false.jpg") 
            self.photo = ImageTk.PhotoImage(img)

        self.rightcanvas.create_image(0,0, anchor = tk.NW, image=self.photo)



if __name__ == "__main__":
    gui = MainGui()
